import glob
import matplotlib.pylab as plt
import seaborn as sns
import matplotlib

import powervisualisation as pv
from time import time

PATH = "../data/new/AdeptDataForPatrick/"
OUTPATH = "../output/AdeptDataForPatrick/"


def set_style():
    # This sets reasonable defaults for font size for
    # a figure that will go in a paper
    sns.set_context("paper")

    # Set the font to be serif, rather than sans
    sns.set(font='serif')

    # Make the background white, and specify the
    # specific font family
    sns.set_style("white", {
        "font.family": "serif",
        "font.serif": ["Times", "Palatino", "serif"]
    })



def files_in_dir(path):
    return glob.glob(path + "/*")

def proc_dataset_grouped(path):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    for experiment in experiments:
        f, ax = plt.subplots(num_runs, sharex=True)
        f.suptitle(path.rsplit("/", 1)[1] + " - " + experiment)

        for ix, run in enumerate(files_in_dir(path)):
            try:
                file = run + "/" + experiment
                T = pv.Trace(file)
                T.power_as_df().plot(title=("/".join(run.rsplit("/",2)[1:]) + "/" + experiment), ax=ax[ix],
                                     legend=False)
                ax[ix].set_title("%s" % run.rsplit("/", 1)[1])

            except ValueError:
                ax[ix].set_title("ERROR: %s" % run.rsplit("/", 1)[1])

        lines, labels = ax[0].get_legend_handles_labels()
        lgd = plt.figlegend(lines, labels, loc='lower center', ncol=5, labelspacing=0.)

        plt.savefig(OUTPATH + "individual/" + dataset.rsplit("/",1)[1] + "/" + experiment + ".SHAREX.pdf",
            type="pdf", orientation="landscape", papertype="a4", bbox_extra_artists=(lgd,))
        plt.close()
        print "SUCCESS", experiment

def proc_dataset_grouped_2(path):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    for experiment in experiments:
        f, ax = plt.subplots(num_runs, sharex=True)
        st = f.suptitle(path.rsplit("/", 1)[1] + " - " + experiment)

        for ix, run in enumerate(files_in_dir(path)):
            file = run + "/" + experiment
            T = pv.Trace(file)
            df = T.power_as_df()
            handles = ax[ix].plot(df.index, df)
            ax[ix].set_title("%s" % run.rsplit("/", 1)[1])
            plt.setp(ax[ix].xaxis.get_majorticklabels(), rotation=70)

        lgd = f.legend(handles, [_[7:] for _ in df.columns], loc = 'center right', bbox_to_anchor=(1, 0.5))
        plt.savefig(OUTPATH + "individual/" + dataset.rsplit("/", 1)[1] + "/" + experiment + ".pdf",
                    type="pdf", orientation="landscape", papertype="a4", bbox_extra_artists=(lgd, st),
                    bbox_inches='tight')
        plt.close()
        print "SUCCESS", experiment

def proc_dataset_indv(path):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    for experiment in experiments:
        for run in files_in_dir(path):
            file = run + "/" + experiment
            T = pv.Trace(file)

            df = T.power_as_df()
            handles = plt.plot(df.index, df)
            plt.title("/".join(run.rsplit("/",2)[1:]) + "/" + experiment)
            lgd = plt.legend(handles, [_[7:] for _ in df.columns], loc='center left', bbox_to_anchor=(1, 0.5))
            plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%M:%S.%f"))


            plt.savefig(OUTPATH + "individual/" + "/".join(run.rsplit("/", 2)[1:]) + "/" +
                                                                  experiment + ".pdf",
                        type="pdf", orientation="landscape", papertype="a4", bbox_extra_artists=(lgd,), \
                                                                                                 bbox_inches='tight')
            plt.close()
            print "SUCCESS", file


def proc_dataset_dram(path):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    for experiment in experiments:
        for run in files_in_dir(path):
            file = run + "/" + experiment
            T = pv.Trace(file)
            df = T.power_as_df()

            cpu = df["sensor_cpu0"] + df["sensor_cpu1"]
            fig, ax1 = plt.subplots()
            ax1.plot(df.index, cpu, color="b")

            ram = df["sensor_dram00"] + df["sensor_dram01"] + df["sensor_dram10"] + df["sensor_dram11"]
            ax2 = ax1.twinx()
            ax2.plot(df.index, ram, color="g")

            plt.title("/".join(run.rsplit("/",2)[1:]) + "/" + experiment)
            plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%M:%S.%f"))


            plt.savefig(OUTPATH + "individual/" + "/".join(run.rsplit("/", 2)[1:]) + "/" +
                                                                 experiment + ".dram.pdf",
                      type="pdf", orientation="landscape", papertype="a4", bbox_inches='tight')
            plt.close()
            print "SUCCESS", file

def proc_dataset_samplediff(path):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    for experiment in experiments:
        for run in files_in_dir(path):
            file = run + "/" + experiment
            T = pv.Trace(file)
            samples = T.samplecount_minmax()
            diff = samples[1] - samples[0]
            if diff > 16384:
                print diff, file

def proc_dataset_describe(path, nth):
    num_runs = len(files_in_dir(path))
    experiments = [_.rsplit("/", 1)[1] for _ in files_in_dir(files_in_dir(path)[0])]

    with open('downsample_aggregation.txt', 'a') as f:
        for experiment in experiments:
            for run in files_in_dir(path):
                file = run + "/" + experiment
                T = pv.Trace(file)
                f.write("%s %s %s\n" % (nth, file, T.aggregate(nth=nth)))
                print "SUCCESS", file

if __name__ == "__main__":

    set_style()
    t1 = time()
    num_files = len(glob.glob(PATH + "/*/*/*"))
    for dataset in files_in_dir(PATH):
        #proc_dataset_samplediff(dataset)
        #proc_dataset_grouped_2(dataset)
        #proc_dataset_dram(dataset)
        # proc_dataset_indv(dataset)
        #proc_dataset_grouped(dataset)
        proc_dataset_describe(dataset, 1)
        proc_dataset_describe(dataset, 10)
        proc_dataset_describe(dataset, 100)
        proc_dataset_describe(dataset, 1000)
        proc_dataset_describe(dataset, 10000)
    t2 = time()
    print "%0.2f seconds passed for %s files" % (t2-t1, num_files)