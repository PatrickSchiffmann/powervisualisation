import glob
import matplotlib.pylab as plt
import seaborn as sns
import pandas as pd
import numpy as np
import powervisualisation as pv


PATH = "../data/Idle2"
OUTPATH = "../output/Idle2/"


def set_style():
    # This sets reasonable defaults for font size for
    # a figure that will go in a paper
    sns.set_context("paper")

    # Set the font to be serif, rather than sans
    sns.set(font='serif')

    # Make the background white, and specify the
    # specific font family
    sns.set_style("white", {
        "font.family": "serif",
        "font.serif": ["Times", "Palatino", "serif"]
    })



def files_in_dir(path):
    return glob.glob(path + "/*")

if __name__ == "__main__":

    set_style()
    res = {}
    for file in files_in_dir(PATH):
        res[file] = pv.Trace(file).aggregate(nth=1)

    results = []
    for filename, filedata in res.iteritems():
        for sensorname, sensordata in filedata.iteritems():
            for aggname, aggdata in sensordata.iteritems():
                results.append([filename, sensorname, aggname, aggdata])

    df = pd.DataFrame(results, columns=["filename", "sensor", "aggregation", "power"])
    df["governor"] = df.filename.map(lambda x: "performance" if "performance" in x else "2400Mhz")
    df["run"] = df.filename.map(lambda x: x[-6])
    sns.factorplot(x="governor", y="power", hue="aggregation", col="sensor",
                   data=df[df.aggregation.isin(["min", "mean", "max"])], col_wrap=3, sharex=False,
                   sharey=False)
    plt.savefig(OUTPATH + "comparison_idle.pdf")
