def all_equal(l):
    """
    Checks if all elements of a list are equal.
    :param l: input arbitrary list
    :return: boolean
    """
    return all(element == l[0] for element in l)
