from __future__ import division

import h5py
import pandas as pd
import numpy as np
import matplotlib.pylab as plt

from powervisualisation.utils import all_equal
from powervisualisation.defaults import *
from powervisualisation.plots import power_spectrum

class Trace:
    def __init__(self, filename):
        self.filename = filename
        self.file = h5py.File(filename, 'r')  # read only

        self.check_format()

    @property
    def sensors(self):
        return self.file.keys()
        #return [key for key in self.file.keys() if ("v" in key or "cpu" in key)]

    @property
    def attributes(self):
        return dict(self.file.attrs.iteritems())

    def check_format(self):
        """
        Asserts file is valid.
        :return: boolean True if format valid
        """

        # Only top level contains groups.
        # Groups start with "sensor_".
        # Each group contains two datsets: current, voltage
        for name, group in self.file.iteritems():
            assert len(group) == 2
            assert name.startswith("sensor_")
            assert CURRENT_NAME in group.keys()
            assert VOLTAGE_NAME in group.keys()

        # All datsets have same shape, implying same sample rate and time measured
        #assert all_equal([group[dataset].shape for dataset in group for group in self.file.values()])
        return True

    def print_samples_by_sensor(self):
        for group in self.file.values():
            print group, [group[dataset].shape for dataset in group]


    @property
    def samplecount(self):
        samplecount = 0
        for group in self.file.values():
            samplecount = max(samplecount, *[group[dataset].shape[0] for dataset in group])
        return samplecount
        #return self.file.values()[0].values()[0].shape[0]

    def samplecount_minmax(self):
        mi, ma = float('inf'), 0
        for group in self.file.values():
            mi = min(ma, *[group[dataset].shape[0] for dataset in group])
            ma = max(ma, *[group[dataset].shape[0] for dataset in group])
        return mi, ma

    @property
    def samplerate(self):
        try:
            samplerate = self.attributes[SAMPLERATE_NAME]
        except KeyError:
            samplerate = DEFAULT_SAMPLE_RATE
        return samplerate

    @property
    def time(self):
        return self.samplecount / self.samplerate

    def get_power(self, sensor, nth=None):

        if nth is None:
            nth = max(1, self.samplecount / DEFAULT_MAX_SAMPLES)

        if not isinstance(nth, int):
            nth = int(nth)

        group = self.file[sensor]
        return group[VOLTAGE_NAME][::nth] * group[CURRENT_NAME][::nth]

    def calc_energy(self, sensor, nth=None):
        """
        Calculates energy of a sensor by integrating power over time.
        :param sensor: string sensor identifier
        :param nth: int downsample
        :return: float energy in Joule
        """
        if nth is None:
            nth = max(1, self.samplecount / DEFAULT_MAX_SAMPLES)

        if not isinstance(nth, int):
            nth = int(nth)

        dt = 1 / self.samplerate * nth
        power = self.get_power(sensor, nth=nth)
        return np.trapz(power, dx=dt)

    def power_as_df(self, nth=None, total=False):
        if nth is None:
            nth = max(1, self.samplecount / DEFAULT_MAX_SAMPLES)

        if not isinstance(nth, int):
            nth = int(nth)

        df = pd.DataFrame()

        for name in self.sensors:
            tmp = pd.DataFrame(self.get_power(name, nth=nth), columns=[name])
            df = df.merge(tmp, left_index=True, right_index=True, how="outer")

        # dt in seconds, ms in milli seconds
        dt = 1 / self.samplerate * nth
        df = df.set_index(pd.DatetimeIndex(start=0, freq=pd.Timedelta("%f s" % (dt)), periods=len(df.index)))
        if total:
            return df.sum(axis=1)
        return df

    def idle_as_df(self, nth=None):
        return self.power_as_df(nth=nth).min()

    def power_corr(self, nth=None):
        if nth is None:
            nth = max(1, self.samplecount / DEFAULT_MAX_SAMPLES)

        if not isinstance(nth, int):
            nth = int(nth)

        return self.power_as_df(nth).corr()

    def play_sensor(self, sensor, rate=44100):
        import scikits.audiolab

        nth = int(self.samplerate / rate)
        power = self.get_power(sensor, nth=nth)

        data = power.astype(np.float64)
        data /= np.max(np.abs(data), axis=0)

        # write array to file:
        scikits.audiolab.wavwrite(power, 'test.wav', fs=rate, enc='pcm16')

        # play the array:
        # scikits.audiolab.play(data, fs=rate)

    def detect_segments(self, sensor=TOTAL_NAME):
        try:
            from powervisualisation.segmentation import detect_breakout
        except ImportError:
            raise ImportError("Segmentation requires Twitter BreakoutDetection library")

        nth = int(max(1, self.samplecount / DEFAULT_SEGMENTATION_SAMPLES))

        if sensor == TOTAL_NAME:
            data = self.power_as_df(nth=nth, total=True).values
        else:
            data = self.get_power(sensor, nth)

        data = np.ascontiguousarray(data, dtype='float')
        return detect_breakout(data, min_change=0.05)

    def aggregate(self, nth=None):
        if nth is None:
            nth = max(1, self.samplecount / DEFAULT_MAX_SAMPLES)

        res = {}
        for sensor in self.sensors:
            power = self.get_power(sensor, nth=nth)
            res[sensor] = {
                "min": np.min(power),
                "max": np.max(power),
                "count": len(power),
                "mean": np.mean(power),
                "std": np.std(power)
            }
        return res

    def plot(self, nth=None):
        power = self.power_as_df(nth)
        plt.figure()
        plt.xticks(rotation=90)

        for col in power:
                line = plt.plot(power.index, power[col].values)
        plt.gca().set_ylim([0, plt.gca().get_ylim()[1]])
        plt.xlabel("Time [s]")
        plt.ylabel("Power [W]")
        plt.legend(power.columns,
                    loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=5)

    def power_spectrum(self, sensor=TOTAL_NAME, nth=None):
        df = self.power_as_df(nth, sensor)
        return power_spectrum(df)
