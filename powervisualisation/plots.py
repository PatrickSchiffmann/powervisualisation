import matplotlib.pylab as plt

import matplotlib.patches as patches
import numpy as np
import seaborn as sns
from scipy.signal import detrend

def power_spectrum(df):
    dt = (df.index[1] - df.index[0]).total_seconds()
    y = detrend(df.values)
    plt.plot(df.index, df.values)
    plt.title("Signal")

    fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)

    Y = np.fft.rfft(y, norm="ortho")
    freq = np.fft.rfftfreq(len(y), dt)
    ax1.plot(freq, np.abs(Y))
    ax2.plot(freq, 10 * np.log10(np.abs(Y) ** 2))
    ax3.plot(freq, np.angle(Y))

    ax3.set_ylim((-np.pi, np.pi))

    ax3.set_xlabel("Frequency [Hz]")

    ax1.set_ylabel("Amplitude Spectrum")
    ax2.set_ylabel("Power [10log10]")
    ax3.set_ylabel("Phase")

def timeseries_plot(df):
    fig, ax = plt.subplots()
    ax.plot(df.index, df)
    ax.set_ylim([0, df.values.max()])
    return fig


def square_pie(data, ax1=None):
    if ax1 is None:
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111, aspect='equal')

    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)

    colors = sns.color_palette(None, len(data))

    total = np.sum(data.values())
    last_origin = [0.0, 0.0]

    for ix, (value, color) in enumerate(zip(sorted(data.values(), reverse=True), colors)):

        if ix % 2 == 0:  # horizontal max
            width = 1.0 - last_origin[0]
            height = (value / total) / (1.0 - last_origin[0])

        else:  # vertical max
            width = (value / total) / (1.0 - last_origin[1])
            height = 1.0 - last_origin[1]

        ax1.add_patch(
            patches.Rectangle(
                last_origin,  # (x,y)
                width,  # width
                height,  # height
                color=color
            )
        )

        if ix % 2 == 0:  # horizontal
            last_origin[1] += height
        else:  # vertical
            last_origin[0] += width

        plt.axis('off')
