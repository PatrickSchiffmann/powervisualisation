from __future__ import division

import matplotlib.pylab as plt
import numpy as np

from BreakoutDetection.Python import breakout_detection


def rescale_list(data):
    """
    Rescales list to interval [0, 1]
    :param data: list of floats
    :return: list of floats
    """
    mi, ma = min(data), max(data)
    return [(val - mi) / (ma - mi) for val in data] if mi != ma else [1] * len(data)


def calc_means(data, breakouts):
    means = [None] * (len(breakouts) + 1)

    for i in range(len(means)):
        left = breakouts[i - 1] if i > 0 else 0
        right = breakouts[i] if i < len(means) - 1 else len(data)
        means[i] = np.mean(data[left:right])

    return means


def enforce_min_change(data, breakouts, min_change):
    means = calc_means(data, breakouts)

    ret = []
    for i in range(len(breakouts)):
        change = abs(means[i] / means[i + 1] - 1)
        if change > min_change:
            ret.append(breakouts[i])
    return ret


def detect_breakout(data, beta=0.001, min_size=24, degree=1, min_change=0.01, plot=True):
    """

    """
    edm = breakout_detection.EdmMulti()
    edm.evaluate(rescale_list(data), beta=beta, min_size=min_size, degree=degree)
    breakouts = edm.getLoc()

    if min_change != 0 and len(breakouts) > 0:
        breakouts = enforce_min_change(data, breakouts, min_change)

    if plot:
        # Draw data
        mi, ma, count = min(data), max(data), len(data)
        f_main = plt.figure(1)
        plt.plot(data)

        f_hists = []
        if len(breakouts) > 0:
            # Draw vertical lines
            for breakout in breakouts:
                plt.axvline(x=breakout, linewidth=2, color="g")
            # Draw horizontal mean lines
            left = 0
            for right in list(breakouts) + [count]:
                mean = np.mean(data[left:right])
                median = np.median(data[left:right])
                x1 = left / count
                x2 = right / count

                f_hist = plt.figure()
                f_hists.append(f_hist)
                plt.hist(data[left:right], range=[mi, ma], bins=100, orientation="horizontal")
                plt.axhline(y=mean, linewidth=5, color='r', ls="dashed")
                plt.axhline(y=median, linewidth=5, color='g', ls="dashed")
                plt.legend(["Mean", "Median"])

                plt.figure(1)
                print x1, x2
                plt.axhline(xmin=x1, xmax=x2, y=mean, linewidth=5, color='r', ls="dashed")
                plt.axhline(xmin=x1, xmax=x2, y=median, linewidth=5, color='g', ls="dashed")
                left = right

        plt.legend(["Data", "Mean", "Median"])
        plt.xlim(0, count)

    return len(breakouts), breakouts, f_main, f_hists
