import pandas as pd
import numpy as np
import matplotlib.pylab as plt

from powervisualisation import Trace

class Multisample:
    def __init__(self, filelist):
        self.filelist = filelist
        self.traces = [Trace(filename) for filename in filelist]

    def plot(self, use_bars=False):
        df = self.power_as_df(mean_only=False)
        power = df.xs("mean", axis=1, level=1)
        power_err = df.xs("std", axis=1, level=1)
        plt.figure()
        plt.xticks(rotation=90)

        for col in power:
            if use_bars:
                plt.errorbar(power.index, power[col].values, yerr=power_err[col])
            else:
                line = plt.plot(power.index, power[col].values)
                plt.fill_between(power.index, power[col].values - power_err[col].values, power[col].values + power_err[
                    col].values, alpha=0.5, color = line[0].get_color())
        plt.gca().set_ylim([0, plt.gca().get_ylim()[1]])
        plt.xlabel("Time [s]")
        plt.ylabel("Power [W]")

    def power_as_df(self, mean_only=True, sample_to="50ms"):
        df = pd.concat([T.power_as_df() for T in self.traces]).resample(sample_to).agg([np.mean, np.std])
        if mean_only:
            return df.xs("mean", axis=1, level=1)
        else:
            return df
