import unittest

from powervisualisation.utils import all_equal


class Test_all_equal(unittest.TestCase):
    def test_ints(self):
        l = [1] * 5
        self.assertTrue(all_equal(l))

        l = [1] * 5 + [2]
        self.assertFalse(all_equal(l))

        l = [2] + [1] * 5
        self.assertFalse(all_equal(l))

    def test_tuples(self):
        l = [(1, 2)] * 5
        self.assertTrue(all_equal(l))

        l = [(1, 2)] * 5 + [(2, 1)]
        self.assertFalse(all_equal(l))
